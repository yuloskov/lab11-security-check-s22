# Homework solution
## Work description
I will check the site [LeetCode](https://leetcode.com/) for the Forgot Password OWASP scenario.

## OWASP checklist
### Return a consistent message for both existent and non-existent accounts.
Valid email case:
|Test Step|Result|
|-|-|
|Open the leetcode.com site| The site is opened |
|Push "Sign in" button| Login form is active|
|Push "Forgot password?" button| Forgot password form is opened|
|Enter valid email and press "Reset my password" | The site sent the email with resetting information|

![Valid email form](./imgs/validEmailForm.png)

Invalid email case:
|Test Step|Result|
|-|-|
|Open the leetcode.com site| The site is opened |
|Push "Sign in" button| Login form is active|
|Push "Forgot password?" button| Forgot password form is opened|
|Enter invalid email and press "Reset my password" | The site says that such an email does not exist|

![Invalid email form](./imgs/invalidEmailForm.png)

The test is not passed. The results for existent and not existent emails are different.

### Ensure that the time taken for the user response message is uniform.
Valid email case:
|Test Step|Result|
|-|-|
|Open the leetcode.com site| The site is opened |
|Push "Sign in" button| Login form is active|
|Push "Forgot password?" button| Forgot password form is opened|
|Enter valid email and press "Reset my password" | The site sent the email with resetting information|
|Check the time of the response| The response time for existing account is 849ms|

Invalid email case:
|Test Step|Result|
|-|-|
|Open the leetcode.com site| The site is opened |
|Push "Sign in" button| Login form is active|
|Push "Forgot password?" button| Forgot password form is opened|
|Enter invalid email and press "Reset my password" | The site writes that such an email does not exist|
|Check the time of the response| The response time for existing account is 350ms|

I did this test 4 times for each of the emails and I got approximately the same results. However it does not matter because the site explicitly writes that such an email does not exist. So, it is a security hole. The test is not passed.

Valid email test:
![Valid email test](./imgs/existentEmailTiming.png)

Invalid email test:
![Invalid email test](./imgs/invalidEmailTiming.png)

### Use a side-channel to communicate the method to reset their password.
|Test Step|Result|
|-|-|
|Open the leetcode.com site| The site is opened |
|Push "Sign in" button| Login form is active|
|Push "Forgot password?" button| Forgot password form is opened|
|Enter valid email and press "Reset my password" | The site sent the email with resetting information|
|Check the mail| I got new message with the resetting link|

Result:
![Email](./imgs/Email.png)

The test is passed

### Use URL tokens for the simplest and fastest implementation
The leetcode.com uses a URL token to reset their password. An example of such token is **12ezt-60i-559555105c70be72c59b**. The token is sufficiently long and random. In different letters the tokens are different. In case of trying to use the same link twice, the site says that the link is already used. Also, only one token is valid at a time and after using the token all the rest tokens are invalid. However, I found that the site can reset the password using not the most recent link - though it is not in OWASP checklist.
Result:
![Bad token](./imgs/badToken.png)


### Do not make a change to the account until a valid token is presented, such as locking out the account
It is hard to find out whether they made some change or not. But I was able to successfully log in with the new password, after using the right token. 